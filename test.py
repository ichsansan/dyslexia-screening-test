import pandas as pd
import numpy as np
import time, os, io, random, string, requests
from pprint import pprint

# Load settings dari file
dfload = pd.read_csv('files/settings.csv')
dfload = dfload.set_index('key')['value']
for i in dfload.index:
    if dfload[i].lower() == 'true':
        dfload[i] = True
    elif dfload[i].lower() == 'false':
        dfload[i] = False
settings = dfload

recv = {'page2-SentencesLocation': 'saya masuk sekolah\r\nsaya makan ayam\r\nsaya rajin belajar\r\n\r\naku pergi kesana',
        'page3-SentencesLocation': 'Saya membaca komik di sekolah pada pagi hari.Jika saya rajin belajar, saya akan pintar.Kalau hari hujan, saya tidak datang.Tadi malam, kamu sudah belajar.Aku akan membantu ibu memasak, kamu bantu ayah membersihkan rumah.',
        'db-userinput': 'files/userinput.csv',
        'page1-vocals': 'aeiou',
        'page2-RandomTexts': 'True',
        'page3-RandomTexts': 'False',
        'page1-consonant': 'bdpgrtcq'}

datasave = pd.Series(recv).sort_index()

# ambil settings dari inputan web
for i in datasave.index:
    if type(settings[i]) == str:
        if settings[i].endswith('.txt'):
            datasave[i] = [f.strip() + '\n' for f in datasave[i].split('\n') if len(f.strip()) > 0]
            print(f'datasave[{i}]:', settings[i], datasave[i])
            
            ff = open(settings[i], 'w+')
            ff.writelines(datasave[i])
            ff.close()
        else:
            settings[i] = datasave[i]
    elif type(settings[i]) == bool:
        if datasave[i].lower() == 'true':
            settings[i] = True
        else: settings[i] = False
    else:
        settings[i] = datasave[i]

# Simpan settings ke file
dfsave = pd.DataFrame(pd.Series(settings)).sort_index().reset_index()
dfsave.columns = ['key','value']
#dfsave.to_csv('settings.csv', index=False)
print('dfsave:')
print(dfsave)


