$(document).ready( function () {
    console.log('Document is ready');
})

function checkdocument(){
    var objnama = document.getElementById('nama');
    if (objnama.value == ""){objnama.classList.add('invalid');}
    else{
        if (objnama.classList.contains('invalid')) {objnama.classList.remove('invalid');}
    }

    var objumur = document.getElementById('umur');
    if (objumur.value == ""){objumur.classList.add('invalid');}
    else{
        if (objumur.classList.contains('invalid')){objumur.classList.remove('invalid');}
    }

}
$('#identitassubmit').click(function() {
    var nama = document.getElementById('nama').value;
    var umur = document.getElementById('umur').value;
    if (nama == ''){ alert('Nama tidak boleh kosong.') }
    else if (umur == ''){ alert('Umur tidak boleh kosong.') }
    else{
        document.getElementById('identitas').className = 'part collapsing';
        document.getElementById('instruksi').className = 'part extending';
    }
})

$('#close-instruction').click(function(){
    document.getElementById('instruksi-content').classList.add('collapse');
    document.getElementById('soal').classList.add('show');

    var nama = document.getElementById('nama').value;
    var umur = document.getElementById('umur').value;

    $.get('/api-text1',{nama:nama, umur:umur}, function(data,status){
        console.log(data);
        for (key in data.data){
            if (key[0] == 'a'){
                document.getElementById(key).src = data.data[key];
            }
            else{
                document.getElementById(key).innerHTML = data.data[key];
            }
        }
    })
})

$('#submit1').click(function(){
    console.log('Submit 1 clicked');
    
    document.getElementById('jwb1a').disabled = true;
    document.getElementById('jwb1b').disabled = true;
    document.getElementById('jwb1c').disabled = true;
    document.getElementById('jwb1d').disabled = true;
    document.getElementById('submit1').hidden = true;

    document.getElementById('soalno2').hidden = false;
    document.getElementById('instruksi-content').hidden = true;

    var nama = document.getElementById('nama').value;
    var umur = document.getElementById('umur').value;
    var sessionid = document.getElementById('sessionid');

    var sa = document.getElementById('soal1a').innerHTML;
    var sb = document.getElementById('soal1b').innerHTML;
    var sc = document.getElementById('soal1c').innerHTML;
    var sd = document.getElementById('soal1d').innerHTML;

    var ja = document.getElementById('jwb1a').value;
    var jb = document.getElementById('jwb1b').value;
    var jc = document.getElementById('jwb1c').value;
    var jd = document.getElementById('jwb1d').value;

    if (nama == ''){alert('Nama tidak boleh kosong')};
    if (umur == ''){alert('Umur tidak boleh kosong')};
    if (sessionid == null){sessionid = ''}
    else {sessionid = sessionid.innerHTML}

    $.get('/api-text2', {nama: nama,
                         umur: umur,
                         sessionid: sessionid,
                         page: 'page1',
                         soal:String([sa,sb,sc,sd]), 
                         jwb:String([ja,jb,jc,jd])}, 
          function(data, status) {
        console.log(data);
        for (key in data.data){
            document.getElementById(key).innerHTML = data.data[key];
        }
        if (data.score == 4) {
            document.getElementById('modalImg').src = '/static/images/ok.png';
            document.getElementById('scoretext').innerHTML = 'Yeeee score anda ' + String(data['score']);
        }
        else{
            document.getElementById('modalImg').src = '/static/images/no.png'
            document.getElementById('scoretext').innerHTML = 'Score anda cuma ' + String(data['score']);
        }
    })
    
    $('#submitModal').modal('show');
})

$('#submit2').click(function(){
    console.log('Submit 2 clicked');
    
    document.getElementById('jwb2a').disabled = true;
    document.getElementById('jwb2b').disabled = true;
    document.getElementById('jwb2c').disabled = true;
    document.getElementById('jwb2d').disabled = true;
    document.getElementById('submit2').hidden = true;

    document.getElementById('soalno3').hidden = false;
    document.getElementById('instruksi-content').hidden = true;

    var nama = document.getElementById('nama').value;
    var umur = document.getElementById('umur').value;
    var sessionid = document.getElementById('sessionid');

    var sa = document.getElementById('soal2a').innerHTML;
    var sb = document.getElementById('soal2b').innerHTML;
    var sc = document.getElementById('soal2c').innerHTML;
    var sd = document.getElementById('soal2d').innerHTML;

    var ja = document.getElementById('jwb2a').value;
    var jb = document.getElementById('jwb2b').value;
    var jc = document.getElementById('jwb2c').value;
    var jd = document.getElementById('jwb2d').value;

    if (nama == ''){alert('Nama tidak boleh kosong')};
    if (umur == ''){alert('Umur tidak boleh kosong')};
    if (sessionid == null){sessionid = ''}
    else {sessionid = sessionid.innerHTML}

    $.get('/api-text3', {nama: nama,
                         umur: umur,
                         sessionid: sessionid,
                         page: 'page2',
                         soal:String([sa,sb,sc,sd]), 
                         jwb:String([ja,jb,jc,jd])}, 
          function(data, status) {
        console.log(data);
        for (key in data.data){
            document.getElementById(key).innerHTML = data.data[key];
        }
        if (data.score == 4) {
            document.getElementById('modalImg').src = '/static/images/ok.png';
            document.getElementById('scoretext').innerHTML = 'Yeeee score anda ' + String(data['score']);
        }
        else{
            document.getElementById('modalImg').src = '/static/images/no.png'
            document.getElementById('scoretext').innerHTML = 'Score anda cuma ' + String(data['score']);
        }
    })
    
    $('#submitModal').modal('show');
})

$('#submit3').click(function(){
    console.log('Submit 3 clicked');
    
    document.getElementById('jwb3a').disabled = true;
    document.getElementById('jwb3b').disabled = true;
    document.getElementById('jwb3c').disabled = true;
    document.getElementById('jwb3d').disabled = true;
    document.getElementById('submit3').hidden = true;

    document.getElementById('soalno4').hidden = false;
    document.getElementById('instruksi-content').hidden = true;

    var nama = document.getElementById('nama').value;
    var umur = document.getElementById('umur').value;
    var sessionid = document.getElementById('sessionid');

    var sa = document.getElementById('soal3a').innerHTML;
    var sb = document.getElementById('soal3b').innerHTML;
    var sc = document.getElementById('soal3c').innerHTML;
    var sd = document.getElementById('soal3d').innerHTML;

    var ja = document.getElementById('jwb3a').value;
    var jb = document.getElementById('jwb3b').value;
    var jc = document.getElementById('jwb3c').value;
    var jd = document.getElementById('jwb3d').value;

    if (nama == ''){alert('Nama tidak boleh kosong')};
    if (umur == ''){alert('Umur tidak boleh kosong')};
    if (sessionid == null){sessionid = ''}
    else {sessionid = sessionid.innerHTML}

    $.get('/api-text4', {nama: nama,
                         umur: umur,
                         sessionid: sessionid,
                         page: 'page3',
                         soal:String([sa,sb,sc,sd]), 
                         jwb:String([ja,jb,jc,jd])}, 
          function(data, status) {
        console.log(data);
        for (key in data.data){
            document.getElementById(key).innerHTML = data.data[key];
        }
        if (data.score == 4) {
            document.getElementById('modalImg').src = '/static/images/ok.png';
            document.getElementById('scoretext').innerHTML = 'Yeeee score anda ' + String(data['score']);
        }
        else{
            document.getElementById('modalImg').src = '/static/images/no.png'
            document.getElementById('scoretext').innerHTML = 'Score anda cuma ' + String(data['score']);
        }
    })
    
    $('#submitModal').modal('show');
})

$('#submit4').click(function(){
    console.log('Submit 4 clicked');
    
    document.getElementById('jwb4a').disabled = true;
    document.getElementById('jwb4b').disabled = true;
    document.getElementById('jwb4c').disabled = true;
    document.getElementById('jwb4d').disabled = true;
    document.getElementById('submit4').hidden = true;

    document.getElementById('soalno4').hidden = false;

    var nama = document.getElementById('nama').value;
    var umur = document.getElementById('umur').value;
    var sessionid = document.getElementById('sessionid');

    var sa = document.getElementById('soal4a').innerHTML;
    var sb = document.getElementById('soal4b').innerHTML;
    var sc = document.getElementById('soal4c').innerHTML;
    var sd = document.getElementById('soal4d').innerHTML;

    var ja = document.getElementById('jwb4a').value;
    var jb = document.getElementById('jwb4b').value;
    var jc = document.getElementById('jwb4c').value;
    var jd = document.getElementById('jwb4d').value;

    if (nama == ''){alert('Nama tidak boleh kosong')};
    if (umur == ''){alert('Umur tidak boleh kosong')};
    if (sessionid == null){sessionid = ''}
    else {sessionid = sessionid.innerHTML}

    $.get('/api-textfinal', {nama: nama,
                         umur: umur,
                         sessionid: sessionid,
                         page: 'page4',
                         soal:String([sa,sb,sc,sd]), 
                         jwb:String([ja,jb,jc,jd])}, 
          function(data, status) {
        console.log(data);
        document.getElementById('nextpage').hidden = false;
        document.getElementById('nextpage1').hidden = false;
    })
    
    $('#submitModal').modal('show');
})

$('#nama').focusout( function(){
    checkdocument();
})

$('#umur').focusout( function(){
    checkdocument();
})
