function refreshtext(){
    $.get('/api_generatepage1', function(data, status) {
        console.log(data);

        if (data.status == 'success'){
            document.getElementById('g1').src = data.data.imgsrc1;
            document.getElementById('j1').name = data.data.text1;

            document.getElementById('g2').src = data.data.imgsrc2;
            document.getElementById('j2').name = data.data.text2;
            
            document.getElementById('g3').src = data.data.imgsrc3;
            document.getElementById('j3').name = data.data.text3;

            document.getElementById('g4').src = data.data.imgsrc4;
            document.getElementById('j4').name = data.data.text4;
        }
        else{
            alert(data.data);
        }
    })
}

function tick(){
    var timer = document.getElementById('timer');
    var time = new Date().getTime();
    
    var x = setInterval(function() {
        var now = new Date().getTime();
        var diff = Math.floor((now - time) / 1000);
        var sec = diff % 60;
        var min = Math.floor(diff / 60);

        if (sec < 10){sec = '0' + String(sec);}
        else{sec = String(sec)}

        if (min < 10){min = '0' + String(min);}
        else{min = String(min)}

        timer.innerHTML = String(min) + ':' + String(sec);
    }, 1000);
}

$('#submitbtn').click( function(){
    console.log('btn clicked');
    $('#mymodal').modal('show');
})

$('#refresh').click( function () {
    refreshtext();
})

$(document).ready( function() {
    console.log('Document is ready');
    refreshtext();
    tick();
} )