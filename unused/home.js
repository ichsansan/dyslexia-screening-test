function refreshtext(){
    $.get('/api_generaterandomdigit', function(data, status) {
        console.log(data);

        if (data.status == 'success'){
            document.getElementById('image1').src = data.data.imgsrc1;
            document.getElementById('image1').name = data.data.text1;

            document.getElementById('image2').src = data.data.imgsrc2;
            document.getElementById('image2').name = data.data.text2;
            
            document.getElementById('image3').src = data.data.imgsrc3;
            document.getElementById('image3').name = data.data.text3;
        }
        else{
            alert(data.data);
        }
    })
}

function refreshword(){
    $.get('/api_generaterandomwords', function(data, status) {
        console.log(data);

        if (data.status == 'success'){
            document.getElementById('imageword1').src = data.data.imgsrc1;
            document.getElementById('imageword1').name = data.data.text1;

            document.getElementById('imageword2').src = data.data.imgsrc2;
            document.getElementById('imageword2').name = data.data.text2;
            
            document.getElementById('imageword3').src = data.data.imgsrc3;
            document.getElementById('imageword3').name = data.data.text3;
        }
        else{
            alert(data.data);
        }
    })
}

function refreshimages(){
    $.get('/api_generaterandomimages', function(data, status) {
        console.log('generaterandomimages');
        console.log(data);

        if (data.status == 'success'){
            document.getElementById('imagerandom1').src = data.data.imgsrc1;
            document.getElementById('imagerandom1').name = data.data.text1;

            document.getElementById('imagerandom2').src = data.data.imgsrc2;
            document.getElementById('imagerandom2').name = data.data.text2;
            
            document.getElementById('imagerandom3').src = data.data.imgsrc3;
            document.getElementById('imagerandom3').name = data.data.text3;
        }
        else{
            alert(data.data);
        }
    })
}

function messagerun() {
    var Messenger = function(el) {
        'use strict';
        var m = this;
    
        m.init = function() {
            m.codeletters = "abcdefghijklmnopqrstuvwxyz";
            m.message = 0;
            m.current_length = 0;
            m.fadeBuffer = false;
            m.messages = [
                'Write the alphabets <b>A</b> to <b>Z</b> in order',
                'Abcdefghijklmnopqrstuvwxyz'
            ];
    
            setTimeout(m.animateIn, 1000);
        };
    
        m.generateRandomString = function(length) {
            var random_text = '';
            while (random_text.length < length) {
                random_text += m.codeletters.charAt(Math.floor(Math.random() * m.codeletters.length));
            }
    
            return random_text;
        };
    
        m.animateIn = function() {
            if (m.current_length < m.messages[m.message].length) {
                m.current_length = m.current_length + 2;
                if (m.current_length > m.messages[m.message].length) {
                    m.current_length = m.messages[m.message].length;
                }
    
                var message = m.generateRandomString(m.current_length);
                $(el).html(message);
    
                setTimeout(m.animateIn, 20);
            } else {
                setTimeout(m.animateFadeBuffer, 20);
            }
        };
    
        m.animateFadeBuffer = function() {
            if (m.fadeBuffer === false) {
                m.fadeBuffer = [];
                for (var i = 0; i < m.messages[m.message].length; i++) {
                    m.fadeBuffer.push({
                        c: (Math.floor(Math.random() * 12)) + 1,
                        l: m.messages[m.message].charAt(i)
                    });
                }
            }
    
            var do_cycles = false;
            var message = '';
    
            for (var i = 0; i < m.fadeBuffer.length; i++) {
                var fader = m.fadeBuffer[i];
                if (fader.c > 0) {
                    do_cycles = true;
                    fader.c--;
                    message += m.codeletters.charAt(Math.floor(Math.random() * m.codeletters.length));
                } else {
                    message += fader.l;
                }
            }
    
            $(el).html(message);
    
            if (do_cycles === true) {
                setTimeout(m.animateFadeBuffer, 50);
            } else {
                setTimeout(m.cycleText, 2000);
            }
        };
    
        m.cycleText = function() {
            m.message = m.message + 1;
            if (m.message >= m.messages.length) {
                m.message = 0;
            }
    
            m.current_length = 0;
            m.fadeBuffer = false;
            $(el).html('');
    
            setTimeout(m.animateIn, 200);
        };
    
        m.init();
    }
    
    console.clear();
    var messenger = new Messenger($('#messenger'));
}


$(document).ready( function() {
    console.log('Document is ready');
    refreshtext();
    refreshword();
    messagerun();
    refreshimages();
} )

$('#submittext').click( function() {
    var name = document.getElementById('Name').value;
    var age = document.getElementById('Age').value;
    var Az = document.getElementById('abjadurut').value;
    var textr1 = document.getElementById('image1').name;
    var textp1 = document.getElementById('inputimage1').value;
    var textr2 = document.getElementById('image2').name;
    var textp2 = document.getElementById('inputimage2').value;
    var textr3 = document.getElementById('image3').name;
    var textp3 = document.getElementById('inputimage3').value;

    console.log('Name: ' + name);
    console.log('Age: ', String(age));
    if (Az.toLowerCase() == 'abcdefghijklmnopqrstuvwxyz'){
        console.log('Order correct!');
        document.getElementById('abjadurut').className = 'form-control valid';
    }
    else{
        console.log('Order incorrect!');
        document.getElementById('abjadurut').className = 'form-control invalid';
    }

    if (textr1 == textp1){
        console.log('text 1 correct!');
        document.getElementById('inputimage1').className = 'form-control valid'
    }
    else{
        console.log('text 1 incorrect!');
        document.getElementById('inputimage1').className = 'form-control invalid'
    }

    if (textr2 == textp2){
        console.log('text 2 correct!');
        document.getElementById('inputimage2').className = 'form-control valid'
    }
    else{
        console.log('text 2 incorrect!');
        document.getElementById('inputimage2').className = 'form-control invalid'
    }

    if (textr3 == textp3){
        console.log('text 3 correct!');
        document.getElementById('inputimage3').className = 'form-control valid'
    }
    else{
        console.log('text 3 incorrect!');
        document.getElementById('inputimage3').className = 'form-control invalid'
    }
})

$('#refreshtext').click( function() {
    console.log('refresh');
    refreshtext();
})

