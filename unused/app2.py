# To start this using:
# "..\..\..\..\SOKET 2\SOKETenv\Scripts\activate"
# py app.py

from flask import Flask, render_template, url_for, request, jsonify, Response, send_file, redirect, session, abort
from flask_login import LoginManager, UserMixin, login_required, login_user, logout_user 
from generator import *
import json

app = Flask(__name__)

app.config.update(
    DEBUG = True,
    SECRET_KEY = 'askodnasodnikn123'
)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "/login"

class User(UserMixin):
    def __init__(self, id):
        self.id = id
        self.name = "user" + str(id)
        self.password = self.name + "_secret"
        
    def __repr__(self):
        return "%d/%s/%s" % (self.id, self.name, self.password)


@app.route('/home')
def home():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    try:
        data = ''
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return render_template('home.html', data=resp)

@app.route('/')
@app.route('/Dyslexia-page-1')
def Dyslexia_page_1():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    
    if len(recv.keys()) > 0:
        SubmitForm(recv)
        session['page1'] = recv
        print('Form submitted')
        resp['status'] = 'success'
        return redirect(url_for('Dyslexia_page_2'))
    else:
        session_id = generate_session()
        resp['data'] = {'SessionID': session_id}
        resp['status'] = 'success'
        return render_template('Dyslexia-test-1.html', data=resp)

@app.route('/Dyslexia-page-2')
def Dyslexia_page_2():
    recv = dict(request.values)
    print('Dyslexia-page-2 ada recv:', recv)
    if recv == {}:
        try: 
            recv = session['page1']
            print('Dyslexia-page-2 ada session:', recv)
        except: return redirect(url_for('Dyslexia_page_2_error'))
    
    resp = {'status':'failed',
            'data':''}

    print('Dyslexia-page-2 recv yg diterima:',recv)
    if recv['PageNumber'] == 'page1':
        resp['data'] = recv 
        resp['status'] = 'success'
    elif recv['PageNumber'] == 'page2':
        SubmitForm(recv)
        session['page2'] = recv
        print('Session 2 submited')
        resp['status'] = 'success'
        return redirect(url_for('Dyslexia_page_3'))
    return render_template('Dyslexia-page-2.html', data=resp)

@app.route('/Dyslexia-page-3')
def Dyslexia_page_3():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    if 'PageNumber' in recv.keys():
        if recv['PageNumber'] == 'page3':
            session['page3'] = recv

    if 'page3' in session.keys():
        recv = session['page3']
        SubmitForm(recv)
        return redirect(url_for('Finish'))
    elif 'page2' in session.keys():
        recv = session['page2']
        resp['data'] = recv
    print(recv)
    return render_template('Dyslexia-page-3.html', data=resp)


@app.route('/Dyslexia-page-2-error')
def Dyslexia_page_2_error():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    return render_template('Dyslexia-page-2_error.html', data=resp)

@app.route('/Finish')
def Finish():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    return render_template('Finish.html', data=resp)

@app.route('/login')
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']        
        if password == username + "_secret":
            id = username.split('user')[1]
            user = User(id)
            login_user(user)
            return redirect(request.args.get("next"))
        else:
            return abort(401)
    else:
        return render_template('login.html')

@app.route('/settings')
#@login_required
def settings():
    return render_template('settings.html')

# ============================================= API lists ============================================= #
@app.route('/api_generatepage1', methods=['GET'])
def api_generatepage1():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    print(recv)
    try:
        if 'number' in recv.keys(): number = recv['number']
        else: number = 3

        data = generate_page1(number=number)
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/api_generatepage2', methods=['GET'])
def api_generatepage2():
    resp = {'status':'failed',
            'data':''}
    try:
        data = generate_page2()
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/api_generatepage3', methods=['GET'])
def api_generatepage3():
    resp = {'status':'failed',
            'data':''}
    try:
        data = generate_page3()
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

# Logout
@app.route("/logout")
@login_required
def logout():
    logout_user()
    return Response('<p>Logged out</p>')

# handle login failed
@app.errorhandler(401)
def page_not_found(e):
    return Response('<p>Login failed</p>')
    
    
# callback to reload the user object        
@login_manager.user_loader
def load_user(userid):
    return User(userid)

if __name__ == '__main__':
    app.run(host='localhost', port='5002')
