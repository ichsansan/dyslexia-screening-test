
def generate_randomdigit():
    # Remove all other images longer than 5 minutes

    letters = string.ascii_lowercase
    text1 =  ''.join(random.choice(letters)+' ' for i in range(4))
    fname1 = generate_word_to_image(text1)

    letters = string.ascii_lowercase
    text2 =  ''.join(random.choice(letters)+' ' for i in range(4))
    fname2 = generate_word_to_image(text2)

    letters = string.ascii_lowercase
    text3 =  ''.join(random.choice(letters)+' ' for i in range(4))
    fname3 = generate_word_to_image(text3)

    data = {'text1': text1.replace(' ',''),
            'imgsrc1': fname1,
            'text2': text2.replace(' ',''),
            'imgsrc2': fname2,
            'text3': text3.replace(' ',''),
            'imgsrc3': fname3}
    return data

def generate_englishwords():
    numberofwords = 3
    link = f'https://random-word-api.herokuapp.com/word?number={numberofwords}&swear=1'

    data = requests.get(link)
    words = data.json()

    images = [generate_word_to_image(f.capitalize(), imgsize=(160,30)) for f in words]
    data = {}
    for i in range(len(words)):
        data[f'text{i+1}'] = words[i]
        data[f'imgsrc{i+1}'] = images[i]
    return data

def generate_indonesianwords():
    numberofwords = 3
    words = np.loadtxt('files/id_words.txt', str)
    np.random.shuffle(words)
    words = words[:numberofwords]
    
    images = [generate_word_to_image(f, imgsize=(160,30)) for f in words]
    data = {}
    for i in range(len(words)):
        data[f'text{i+1}'] = words[i]
        data[f'imgsrc{i+1}'] = images[i]
    return data

def generate_randomimages():
    numberofimage = 3
    imagepath = 'static/images/randomimages'
    names = [f for f in os.listdir(imagepath) if f.endswith('.png')]
    names = names[:numberofimage]

    images = [os.path.join(imagepath, f) for f in names]

    data = {}
    for i in range(len(names)):
        data[f'text{i+1}'] = names[i]
        data[f'imgsrc{i+1}'] = images[i]
    return data