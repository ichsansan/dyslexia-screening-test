# ReadMe

In this project, I create a web system that can calculate the Dyslexia score on users. The targetted users is Elementary School students.
The projects are run on Python 3.7.6 with Flask 2.0.1 as the main environment. This project is currently on progress, so the files will be updated without notifications.

I have separeted folders used, and files that I actually unused, but I hesitate to remove it permanently. So all unused files were saved on `/unused` folder.

To start this project, you just have to run `app.py` on console, and the program starts running on your machine.
