# To start this using:
# "..\..\..\..\SOKET 2\SOKETenv\Scripts\activate"
# py app.py
#
# To push on github:
# git remote add origin https://gitlab.com/ichsansan/dyslexia-screening-test.git
# git branch -M main
# git push -uf origin main

from flask import Flask, render_template, url_for, request, jsonify, Response, send_file, redirect, session, abort
from flask.helpers import send_from_directory
from flask.signals import Namespace
from flask_login import LoginManager, UserMixin, login_required, login_user, logout_user 
from generator import *
from pprint import pprint
import json

app = Flask(__name__)

app.config.update(
    DEBUG = True,
    SECRET_KEY = 'abcdefgh123'
)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "/login"

class User(UserMixin):
    def __init__(self, id):
        self.id = id
        self.name = "user" + str(id)
        self.password = self.name + "_secret"
        
    def __repr__(self):
        return "%d/%s/%s" % (self.id, self.name, self.password)

@app.route('/')
@app.route('/Dyslexia-page-1')
def Dyslexia_page_1():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    
    if len(recv.keys()) > 0:
        SubmitForm(recv)
        session['page1'] = recv
        print('Form submitted')
        resp['status'] = 'success'
        return redirect(url_for('Dyslexia_page_2'))
    else:
        session_id = generate_session()
        session['id'] = session_id
        resp['data'] = {'SessionID': session_id}
        resp['status'] = 'success'
        return render_template('Dyslexia-test-1.html', data=resp)

@app.route('/Dyslexia-page-2')
def Dyslexia_page_2():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    print(recv)

    if recv == {}:
        session_id = session['id']
        resp['data'] = {'SessionID': session_id,
                        'Nama': session['nama']}
        resp['status'] = 'success'
        return render_template('Dyslexia-page-2.html', data=resp)
    else:
        recv['Umur'] = session['umur']
        SubmitForm(recv)
        return redirect(url_for('Dyslexia_page_3'))

@app.route('/Dyslexia-page-3')
def Dyslexia_page_3():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    print(recv)

    if recv == {}:
        session_id = session['id']
        resp['data'] = {'SessionID': session_id,
                        'Nama': session['nama']}
        resp['status'] = 'success'
        return render_template('Dyslexia-page-3.html', data=resp)
    else:
        recv['Umur'] = session['umur']
        SubmitForm(recv)
        return redirect(url_for('Dyslexia_finish'))

@app.route('/Dyslexia-finish')
def Dyslexia_finish():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    print(recv)

    return render_template('finish.html')

@app.route('/settings-page')
def settings_page():
    recv = dict(request.values)
    resp = {'status':'failed',
            'data':''}
    
    if recv == {} or 'updated' in recv.keys():
        data = generate_settings()
        if 'updated' in recv.keys():
            resp['message'] = 'Data berhasil diupdate!'
        resp['data'] = data
        resp['status'] = 'success'
    else:
        save_settings(recv)
        data = generate_settings()
        resp['data'] = data
        resp['message'] = 'Settings updated!'
        resp['status'] = 'success'
        return redirect( url_for('settings_page', updated=True ))
    print('resp:',resp)
    return render_template('settings.html', data=resp)

# -------------------------------- API List -------------------------------- #
@app.route('/api-text1', methods=['GET'])
def api_text1():
    recv = dict(request.values)
    resp = {'status': 'failed',
            'data': ''}
    print(recv)
    try:
        data = {}
        session['nama'] = recv['nama']
        session['umur'] = recv['umur']

        for i in ['a','b','c','d']:
            data[f'soal1{i}'] = generator_digit(2)
        data['audio1src'] = settings['page1-audio1']
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/api-text2', methods=['GET'])
def api_text2():
    recv = dict(request.values)
    resp = {'status': 'failed',
            'data': ''}
    print(recv)
    try:
        data = {}
        score = SubmitAPI(recv)
        
        for i in ['a','b','c','d']:
            data[f'soal2{i}'] = generator_digit(3)
        resp['score'] = score
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/api-text3', methods=['GET'])
def api_text3():
    recv = dict(request.values)
    resp = {'status': 'failed',
            'data': ''}
    print(recv)
    try:
        data = {}
        score = SubmitAPI(recv)
        
        for i in ['a','b','c','d']:
            data[f'soal3{i}'] = generator_digit(4)
        resp['score'] = score
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/api-text4', methods=['GET'])
def api_text4():
    recv = dict(request.values)
    resp = {'status': 'failed',
            'data': ''}
    print(recv)
    try:
        data = {}
        score = SubmitAPI(recv)
        
        for i in ['a','b','c','d']:
            data[f'soal4{i}'] = generator_digit(5)
        resp['score'] = score
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/api-textfinal', methods=['GET'])
def api_textfinal():
    recv = dict(request.values)
    resp = {'status': 'failed',
            'data': ''}
    print(recv)
    try:
        data = 'OK'
        score = SubmitAPI(recv)
        
        resp['score'] = score
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/api_generatepage2', methods=['GET'])
def api_generatepage2():
    resp = {'status':'failed',
            'data':''}
    try:
        data = generate_page2()
        data['Nama'] = session['nama']
        data['SessionID'] = session['id']

        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/api_generatepage3', methods=['GET'])
def api_generatepage3():
    resp = {'status':'failed',
            'data':''}
    try:
        data = generate_page3()
        resp['data'] = data
        resp['status'] = 'success'
    except Exception as e:
        resp['data'] = str(e)
    return jsonify(resp)

@app.route('/audio1')
def api_audio1():
    return send_file('files/audio/AudioTest.mp3')
    return send_from_directory('/files/audio','AudioTest.mp3', as_attachment=False)

# Logout
@app.route("/logout")
@login_required
def logout():
    logout_user()
    return Response('<p>Logged out</p>')

# handle login failed
@app.errorhandler(401)
def page_not_found(e):
    return Response('<p>Login failed</p>')
    
    
# callback to reload the user object        
@login_manager.user_loader
def load_user(userid):
    return User(userid)

if __name__ == '__main__':
    app.run(host='localhost', port='5002')
