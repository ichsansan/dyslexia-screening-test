from PIL import Image, ImageDraw, ImageFont
import pandas as pd
import numpy as np
import time, os, io, random, string, requests
 
settings = pd.read_csv('files/settings.csv')
settings = settings.set_index('key')['value']

for i in settings.index:
    if settings[i].lower() == 'true':
        settings[i] = True
    elif settings[i].lower() == 'false':
        settings[i] = False

def generate_word_to_image(text, imgsize = (100,30), fontsize = 24):
    text = str(text)
    startingpoint = (2, 2)
    if fontsize < 18: startingpoint = tuple(np.array(startingpoint) + 8)

    figpath = 'static/images/temp'
    remove = [os.remove(f"{figpath}/{f}") for f in os.listdir(figpath) if (time.time() - os.stat(f"{figpath}/{f}").st_ctime) > 300]

    time.sleep(0.2)
    img = Image.new('RGB', imgsize, (255,255,255))
    font = ImageFont.truetype("arial", fontsize)
    d = ImageDraw.Draw(img)
    d.text(startingpoint, text, font=font, fill=(47, 139, 79))
    fname = figpath + '/' + text.replace(' ','') + '.png'
    img.save(fname)
    
    return fname

def generator_digit(digitcount = 3):
    vocals = list(settings['page1-vocals'])
    cons = list(settings['page1-consonant'])

    word = ''
    for w in range(digitcount):
        if w%2: word += vocals[np.random.randint(len(vocals))]
        else: word += cons[np.random.randint(len(cons))]
    return word

def generate_page1(number=1):
    textcount = 4
    digitcount = number + 1
    words = [generator_digit(digitcount) for f in range(textcount)]
    images = [generate_word_to_image(t) for t in words]

    data = {}

    for i in range(len(words)):
        data[f'text{i+1}'] = words[i]
        data[f'imgsrc{i+1}'] = images[i]
    print(data)
    return data

def generate_page2():
    textcount = 4

    texts = np.loadtxt(settings['page2-SentencesLocation'], dtype=str)
    if settings['page2-RandomTexts']:
        words = np.array([random.choice(texts) for f in range(textcount)])
    else:
        words = texts[:textcount]

    soal = []
    for i in range(len(words)):
        s = ''
        for j in range(len(words[i])):
            s += words[i][j] + ' '
        s = s[:-1]
        soal.append(s)

    soalshuf = words.copy()
    [np.random.shuffle(soalshuf[i]) for i in range(len(soalshuf))]
    soalshuffle = []
    for i in range(len(soalshuf)):
        s = ''
        for j in range(len(soalshuf[i])):
            s += soalshuf[i][j] + ' - '
        s = s[:-3]
        soalshuffle.append(s)
    
    images = [generate_word_to_image(f, imgsize=(300,30), fontsize=18) for f in soalshuffle]

    data = {}
    for i in range(len(soal)):
        data[f'text{i+1}'] = soal[i]
        data[f'imgsrc{i+1}'] = images[i]
    return data

def generate_page3():
    textcount = 4

    texts = np.loadtxt(settings['page3-SentencesLocation'], dtype=str, delimiter='|')
    if settings['page3-RandomTexts']:
        words = np.array([random.choice(texts) for f in range(textcount)])
    else:
        words = texts[:textcount]

    images = [generate_word_to_image(f, imgsize=(500,30), fontsize=13) for f in words]

    data = {}
    for i in range(len(words)):
        data[f'text{i+1}'] = words[i]
        data[f'imgsrc{i+1}'] = images[i]
    return data

def generate_settings():
    keys = list(np.sort(list(settings.keys())))
    values = [settings[k] for k in keys]
    types = []

    for i,v in enumerate(values):
        if type(v) == str:
            if os.path.isfile(v) and v.endswith('.txt'):
                f = open(v)
                values[i] = ''.join(f for f in f.readlines())
                types.append('textarea')
            else: types.append('inputtext')
        elif type(v) == bool:
            types.append('checkbox')
            values[i] = str(values[i])
        else: types.append('inputtext')

    data = {'keys': keys,
            'values': values,
            'types':types}
    return data

def save_settings(recv):
    global settings
    datasave = pd.Series(recv).sort_index()

    # ambil settings dari inputan web
    for i in datasave.index:
        if type(settings[i]) == str:
            if settings[i].endswith('.txt'):
                datasave[i] = [f.strip() + '\n' for f in datasave[i].split('\n') if len(f.strip()) > 0]
                
                ff = open(settings[i], 'w+')
                ff.writelines(datasave[i])
                ff.close()
            else:
                settings[i] = datasave[i]
        elif type(settings[i]) == bool:
            if datasave[i].lower() == 'true':
                settings[i] = True
            else: settings[i] = False
        else:
            settings[i] = datasave[i]
    
    # Simpan settings ke file
    dfsave = pd.DataFrame(pd.Series(settings)).sort_index().reset_index()
    dfsave.columns = ['key','value']
    dfsave.to_csv('files/settings.csv', index=False)

    # Load settings dari file
    dfload = pd.read_csv('files/settings.csv')
    dfload = dfload.set_index('key')['value']
    for i in dfload.index:
        if dfload[i].lower() == 'true':
            dfload[i] = True
        elif dfload[i].lower() == 'false':
            dfload[i] = False
    settings = dfload
    return 'success'

def generate_session():
    letters = string.ascii_letters + string.digits
    sess_name =  ''.join(random.choice(letters) for i in range(12))
    return sess_name

def SubmitAPI(data):
    soal = data['soal'].split(',')
    jwb = data['jwb'].split(',')

    nama = data['nama']
    umur = data['umur']
    sessionid = data['sessionid']
    page = data['page']
    wkt = time.time()

    data = []
    score = 0
    for i in range(len(soal)):
        if soal[i] == jwb[i]:
            score += 1
        data.append([wkt, sessionid, page, nama, umur, soal[i],jwb[i]])
    data.append([wkt, sessionid, page, nama, umur, 'score',score])

    df = pd.DataFrame(data = data,
                    columns=['Time','SessionID','PageNumber','Nama','Umur','Soal','Jawaban'])
    db = pd.read_csv(settings['db-userinput'])
    db = db.append(df)
    db.to_csv(settings['db-userinput'], index=False)
    return score

def SubmitForm(data):
    DF = pd.DataFrame(np.array(list(data.items()))).set_index(0).T.iloc[0]
    DataUser = DF[['Nama','Umur','PageNumber','SessionID']]

    DF = pd.DataFrame(DF)
    DF = DF.drop(index=DataUser.index).reset_index()
    DF.columns = ['Soal','Jawaban']

    DataUser['Time'] = int(time.time())
    for i in DataUser.index:
        DF[i] = DataUser[i]

    db = pd.read_csv(settings['db-userinput'])
    db = db.append(DF)
    db.to_csv(settings['db-userinput'], index=False)
    
    return None